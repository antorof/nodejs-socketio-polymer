var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'My Components' });
});
router.get('/example-app', function(req, res, next) {
  res.render('example-app', { title: 'Example App', user: 'Fernando' });
});
router.get('/example-app/:user', function(req, res, next) {
  res.render('example-app', { title: 'Example App', user: req.params.user });
});
router.get('/tests', function(req, res, next) {
  res.render('tests', { title: 'Tests' });
});
router.get('/individual', function(req, res, next) {
  res.render('individual', { title: 'Test Individual' });
});
router.get('/chat', function(req, res, next) {
  res.render('chat', { title: 'Chat' });
});
router.get('/prev', function(req, res, next) {
  res.render('index-prev', { title: 'Index - PREV' });
});

router.get('/:nombre', function(req, res, next) {
  res.render('index', { title: 'Express', nombre: req.params.nombre });
});

router.post('/', function(req, res){
  var $_POST = JSON.stringify(req.body,null,4);
  res.render('index', { title: 'My Components', posted: $_POST});
});

router.post('/tests', function(req, res){
  var $_POST = JSON.stringify(req.body,null,4);
  res.render('tests', { title: 'Tests', posted: $_POST});
});

module.exports = router;
