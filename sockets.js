module.exports = function(io) {
  console.log("-- SOCKET.IO STARTED");

  var connectedUsers = {};

  io.sockets.on('connection', function (socket) {
    console.log("[io] NUEVA CONEXION");

    socket.emit("connection-established","Hello visitor");

    //var connectedUsersId = [];
    //for (var i in socket.nsp.connected) {
    //  connectedUsersId.push(i);
    //}
    ////console.log(connectedUsersId);
    //io.sockets.emit("connected-users", connectedUsersId);

    socket.on('component-event', function(data) {
      console.log(data);
      io.sockets.emit('event-transmission',data);
    });

    socket.on('disconnect', function(){
      console.log("adios, "+socket.id);

      delete connectedUsers[socket.id];
      //var connectedUsersId = [];
      //for (var i in socket.nsp.connected) {
      //  connectedUsersId.push(i);
      //}
      //console.log(connectedUsersId);
      io.sockets.emit("connected-users", connectedUsers);
    });

    socket.on('identification', function (data) {
      console.log("hola, "+data)
      console.log(socket.id)

      connectedUsers[socket.id] = data;

      //var connectedUsersId = [];
      //for (var i in socket.nsp.connected) {
      //  connectedUsersId.push(i);
      //}
      ////console.log(connectedUsersId);
      io.sockets.emit("connected-users", connectedUsers);
    });
  });
};